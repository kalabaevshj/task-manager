package uz.aliftech.taskmanager.ui.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import uz.aliftech.taskmanager.R
import uz.aliftech.taskmanager.databinding.FragmentMainBinding

class MainFragment : Fragment(R.layout.fragment_main) {

    private lateinit var binding: FragmentMainBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMainBinding.bind(view)
        setUpBottomNavigation()
    }

    private fun setUpBottomNavigation() {
        val navController = Navigation.findNavController(requireActivity(), R.id.main_nav_host)
        NavigationUI.setupWithNavController(binding.bottomNavigation, navController)
    }
}