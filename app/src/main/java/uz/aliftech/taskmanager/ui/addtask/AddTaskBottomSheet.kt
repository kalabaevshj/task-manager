package uz.aliftech.taskmanager.ui.addtask

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.koin.android.ext.android.inject
import uz.aliftech.taskmanager.R
import uz.aliftech.taskmanager.base.dateToLong
import uz.aliftech.taskmanager.base.onClick
import uz.aliftech.taskmanager.base.showMessage
import uz.aliftech.taskmanager.data.model.Task
import uz.aliftech.taskmanager.databinding.BottomSheetAddTaskBinding
import uz.aliftech.taskmanager.base.ContentDisplayer
import java.text.SimpleDateFormat
import java.util.*

class AddTaskBottomSheet(private val contentDisplayer: ContentDisplayer) :
    BottomSheetDialogFragment() {

    private lateinit var binding: BottomSheetAddTaskBinding
    private lateinit var dateListener: DatePickerDialog.OnDateSetListener
    private lateinit var datePickerDialog: DatePickerDialog
    private val viewModel: AddTaskViewModel by inject()
    private val calendar = Calendar.getInstance()
    private var isDeadlineSelected: Boolean = false
    private var openedForUpdate: Boolean = false
    private lateinit var task: Task

    companion object {
        const val TAG = "AddTaskBottomSheet"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setUpObservers()
        return inflater.inflate(R.layout.bottom_sheet_add_task, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = BottomSheetAddTaskBinding.bind(view)
        dateListener = DatePickerDialog.OnDateSetListener { _, year, month, day ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, month)
            calendar.set(Calendar.DAY_OF_MONTH, day)
            isDeadlineSelected = true
            updateDeadline()
        }

        datePickerDialog = DatePickerDialog(
            requireContext(),
            dateListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        binding.apply {
            taskName.requestFocus()
            datePicker.onClick {
                datePickerDialog.show()
            }
            deadlineText.onClick { datePicker.callOnClick() }
            saveButton.onClick {
                when {
                    taskName.text.isNullOrEmpty() -> {
                        showMessage(requireContext().getString(R.string.please_input_task))
                    }
                    !isDeadlineSelected -> {
                        showMessage(requireContext().getString(R.string.please_select_deadline))
                    }
                    else -> {
                        if (openedForUpdate) {
                            task.name = taskName.text.toString()
                            task.deadline = deadlineText.text.toString()
                                .dateToLong(requireContext().getString(R.string.date_format))
                            viewModel.updateTask(task)
                        } else {
                            viewModel.addNewTask(
                                Task(
                                    name = taskName.text.toString(),
                                    deadline = deadlineText.text.toString()
                                        .dateToLong(requireContext().getString(R.string.date_format))
                                )
                            )
                        }
                    }
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (openedForUpdate && ::task.isInitialized) {
            calendar.timeInMillis = task.deadline
            binding.taskName.setText(task.name)
            isDeadlineSelected = true
            updateDeadline()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        openedForUpdate = false
        isDeadlineSelected = false
        binding.taskName.text.clear()
    }

    private fun setUpObservers() {
        viewModel.newTask.observe(viewLifecycleOwner) {
            binding.apply {
                taskName.text.clear()
                deadlineText.text = requireContext().getString(R.string.select_date)
            }
            contentDisplayer.display()
        }
        viewModel.updatedTask.observe(viewLifecycleOwner) {
            contentDisplayer.display()
            this.dismiss()
        }
    }

    fun showForUpdate(fragmentManager: FragmentManager, mTask: Task) {
        openedForUpdate = true
        this.task = mTask
        show(fragmentManager, TAG)
    }

    private fun updateDeadline() {
        val dateFormat =
            SimpleDateFormat(requireContext().getString(R.string.date_format), Locale.ROOT)
        binding.deadlineText.text = dateFormat.format(calendar.time)
    }
}