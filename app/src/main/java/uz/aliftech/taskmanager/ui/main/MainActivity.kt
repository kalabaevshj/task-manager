package uz.aliftech.taskmanager.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import uz.aliftech.taskmanager.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}