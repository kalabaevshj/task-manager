package uz.aliftech.taskmanager.ui.addtask

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import uz.aliftech.taskmanager.base.SingleLiveEvent
import uz.aliftech.taskmanager.data.model.Task
import uz.aliftech.taskmanager.domain.usecase.TaskUseCase

class AddTaskViewModel(
    private val useCase: TaskUseCase,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    private var _newTask = SingleLiveEvent<Task>()
    val newTask = _newTask

    private var _updatedTask = SingleLiveEvent<Task>()
    val updatedTask = _updatedTask

    fun addNewTask(task: Task) {
        viewModelScope.launch(dispatcher) {
            useCase.addTask(task)
            _newTask.postValue(task)
        }
    }

    fun updateTask(task: Task) {
        viewModelScope.launch(dispatcher) {
            useCase.updateTask(task)
            _updatedTask.postValue(task)
        }
    }
}