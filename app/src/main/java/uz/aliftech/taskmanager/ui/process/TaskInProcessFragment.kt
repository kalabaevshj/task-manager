package uz.aliftech.taskmanager.ui.process

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import org.koin.android.ext.android.inject
import uz.aliftech.taskmanager.base.BaseTasksFragment
import uz.aliftech.taskmanager.base.Status
import uz.aliftech.taskmanager.ui.addtask.AddTaskBottomSheet
import uz.aliftech.taskmanager.base.ContentDisplayer
import uz.aliftech.taskmanager.ui.main.TaskInProcessViewModel

class TaskInProcessFragment: BaseTasksFragment(), ContentDisplayer {

    private val viewModel: TaskInProcessViewModel by inject()
    private val addTaskBottomSheet = AddTaskBottomSheet(this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter.onTaskStatusChanged = {
            viewModel.updateTask(it)
        }
        adapter.onTaskRemove = {
            viewModel.removeTask(it)
        }
        adapter.onTaskEdit = {
            addTaskBottomSheet.showForUpdate(childFragmentManager, it)
        }
        binding.addTask.hide()
        setUpObservers()
        display()
    }

    private fun setUpObservers() {
        viewModel.inProcessTasks.observe(viewLifecycleOwner) {
            when (it.status) {
                Status.LOADING -> {
                    binding.progressBar.isVisible = true
                }
                Status.SUCCESS -> {
                    binding.progressBar.isVisible = false
                    adapter.models = it.data!!
                }
            }
        }

        viewModel.updatedTask.observe(viewLifecycleOwner) {
            adapter.update(it)
            display()
        }

        viewModel.removedTask.observe(viewLifecycleOwner) {
            adapter.remove(it)
        }
    }

    override fun display() {
        viewModel.getInProgressTasksOrderByDeadline()
    }
}