package uz.aliftech.taskmanager.ui.done

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import org.koin.android.ext.android.inject
import uz.aliftech.taskmanager.base.BaseTasksFragment
import uz.aliftech.taskmanager.base.Status

class TaskDoneFragment: BaseTasksFragment() {

    private val viewModel: TaskDoneViewModel by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter.onTaskRemove = {
            viewModel.removeTask(it)
        }
        viewModel.getDoneTaskListOrderByDeadline()
        binding.addTask.hide()
        setUpObservers()
    }

    private fun setUpObservers() {
        viewModel.taskList.observe(viewLifecycleOwner) {
            when(it.status) {
                Status.LOADING -> {
                    binding.progressBar.isVisible = true
                }
                Status.SUCCESS -> {
                    binding.progressBar.isVisible = false
                    adapter.models = it.data!!
                }
            }
        }
        viewModel.removedTask.observe(viewLifecycleOwner) {
            adapter.remove(it)
        }
    }
}