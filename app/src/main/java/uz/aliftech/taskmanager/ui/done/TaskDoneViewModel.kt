package uz.aliftech.taskmanager.ui.done

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import uz.aliftech.taskmanager.base.Resource
import uz.aliftech.taskmanager.base.SingleLiveEvent
import uz.aliftech.taskmanager.data.model.Task
import uz.aliftech.taskmanager.domain.usecase.TaskUseCase

class TaskDoneViewModel(
    private val useCase: TaskUseCase,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    private var _taskList = MutableLiveData<Resource<List<Task>>>()
    val taskList: LiveData<Resource<List<Task>>> = _taskList

    private var _removedTask = SingleLiveEvent<Task>()
    val removedTask = _removedTask

    fun getDoneTaskListOrderByDeadline() {
        _taskList.value = Resource.loading()
        viewModelScope.launch(dispatcher) {
            _taskList.postValue(Resource.success(useCase.getTasksByStatusOrderByDeadline(Task.DONE)))
        }
    }

    fun removeTask(task: Task) {
        viewModelScope.launch(dispatcher) {
            useCase.removeTask(task)
            _removedTask.postValue(task)
        }
    }
}