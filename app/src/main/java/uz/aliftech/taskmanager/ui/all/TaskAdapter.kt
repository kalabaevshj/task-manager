package uz.aliftech.taskmanager.ui.all

import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.aliftech.taskmanager.R
import uz.aliftech.taskmanager.base.inflate
import uz.aliftech.taskmanager.base.onClick
import uz.aliftech.taskmanager.base.toDate
import uz.aliftech.taskmanager.data.model.Task
import uz.aliftech.taskmanager.databinding.ItemTaskBinding
import java.util.*

class TaskAdapter : RecyclerView.Adapter<TaskAdapter.TaskViewHolder>() {

    private var _models: MutableList<Task> = mutableListOf()
    var models: List<Task>
        set(value) {
            _models = value.toMutableList()
            notifyDataSetChanged()
        }
        get() = _models

    var onTaskStatusChanged: (Task) -> Unit = {}
    var onTaskEdit: (Task) -> Unit = {}
    var onTaskRemove: (Task) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val view = parent.inflate(R.layout.item_task)
        val binding = ItemTaskBinding.bind(view)
        return TaskViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.populateModel(models[position])
    }

    override fun getItemCount() = models.size

    fun update(task: Task) {
        val oldTask = models.find { it.id == task.id }
        val position = models.indexOf(oldTask)
        _models[position] = task
        notifyItemChanged(position)
    }

    fun remove(task: Task) {
        val position = _models.indexOf(task)
        _models.removeAt(position)
        notifyItemRemoved(position)
    }

    inner class TaskViewHolder(private val binding: ItemTaskBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val calendar = Calendar.getInstance()

        fun populateModel(model: Task) {
            binding.apply {
                taskNameText.text = model.name
                deadlineText.text =
                    model.deadline.toDate(root.context.getString(R.string.date_format))
                if (getTodayInMilliseconds() > model.deadline && model.status != Task.DONE) {
                    deadlineText.setTextColor(Color.RED)
                } else {
                    deadlineText.setTextColor(Color.BLACK)
                }
                binding.editButton.visibility =
                    if (model.status == Task.DONE) {
                        View.GONE
                    } else {
                        View.VISIBLE
                    }
                beginButton.apply {
                    setImageResource(
                        when (model.status) {
                            Task.IN_PROCESS -> R.drawable.ic_baseline_timelapse_24
                            Task.DONE -> R.drawable.ic_baseline_done_all_24
                            else -> R.drawable.ic_baseline_play_circle_outline_24
                        }
                    )
                    onClick {
                        model.status =
                            when (model.status) {
                                Task.TODO -> Task.IN_PROCESS
                                else -> Task.DONE
                            }
                        onTaskStatusChanged.invoke(model)
                    }
                }
                editButton.onClick {
                    onTaskEdit.invoke(model)
                }
                removeButton.onClick {
                    onTaskRemove.invoke(model)
                }
            }
        }

        private fun getTodayInMilliseconds(): Long {
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val date = calendar.get(Calendar.DATE)
            calendar.clear()
            calendar.set(year, month, date)
            return calendar.timeInMillis
        }
    }
}