package uz.aliftech.taskmanager.ui.all

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import uz.aliftech.taskmanager.base.Resource
import uz.aliftech.taskmanager.base.SingleLiveEvent
import uz.aliftech.taskmanager.data.model.Task
import uz.aliftech.taskmanager.domain.usecase.TaskUseCase

class AllTasksViewModel(
    private val useCase: TaskUseCase,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {
    private var _allTasks = MutableLiveData<Resource<List<Task>>>()
    val allTasks: LiveData<Resource<List<Task>>> = _allTasks

    private var _updatedTask = SingleLiveEvent<Task>()
    val updatedTask: LiveData<Task> = _updatedTask

    private var _removedTask = SingleLiveEvent<Task>()
    val removedTask = _removedTask

    fun getAllTasksOrderByDeadline() {
        _allTasks.value = Resource.loading()
        viewModelScope.launch(dispatcher) {
            _allTasks.postValue(Resource.success(useCase.getAllTasksOrderByDeadline()))
        }
    }

    fun updateTask(task: Task) {
        viewModelScope.launch(dispatcher) {
            useCase.updateTask(task)
            _updatedTask.postValue(task)
        }
    }

    fun removeTask(task: Task) {
        viewModelScope.launch(dispatcher) {
            useCase.removeTask(task)
            _removedTask.postValue(task)
        }
    }
}