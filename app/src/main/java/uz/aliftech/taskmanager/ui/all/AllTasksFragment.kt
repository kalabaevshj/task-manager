package uz.aliftech.taskmanager.ui.all

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import org.koin.android.ext.android.inject
import uz.aliftech.taskmanager.base.*
import uz.aliftech.taskmanager.ui.addtask.AddTaskBottomSheet

class AllTasksFragment : BaseTasksFragment(), ContentDisplayer {

    private val addTaskBottomSheet = AddTaskBottomSheet(this)
    private val viewModel: AllTasksViewModel by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {

            adapter.onTaskStatusChanged = {
                viewModel.updateTask(it)
            }
            adapter.onTaskEdit = {
                addTaskBottomSheet.showForUpdate(childFragmentManager, it)
            }
            adapter.onTaskRemove = {
                viewModel.removeTask(it)
            }

            taskList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    // if the recycler view is scrolled
                    // above hide the FAB
                    if (dy > 10 && addTask.isShown) {
                        addTask.hide()
                    }

                    // if the recycler view is
                    // scrolled above show the FAB
                    if (dy < -10 && !addTask.isShown) {
                        addTask.show()
                    }

                    // of the recycler view is at the first
                    // item always show the FAB
                    if (!recyclerView.canScrollVertically(-1)) {
                        addTask.show()
                    }
                }
            })

            setUpObservers()
            display()
            addTask.onClick {
                addTaskBottomSheet.show(
                    childFragmentManager,
                    AddTaskBottomSheet.TAG
                )
            }

        }
    }

    override fun onResume() {
        super.onResume()
        binding.addTask.isVisible = true
    }

    private fun setUpObservers() {
        viewModel.allTasks.observe(viewLifecycleOwner) {
            when (it.status) {
                Status.LOADING -> {
                    binding.progressBar.isVisible = true
                }
                Status.SUCCESS -> {
                    binding.progressBar.isVisible = false
                    adapter.models = it.data!!
                }
            }
        }

        viewModel.updatedTask.observe(viewLifecycleOwner) {
            adapter.update(it)
        }

        viewModel.removedTask.observe(viewLifecycleOwner) {
            adapter.remove(it)
        }
    }

    override fun display() {
        viewModel.getAllTasksOrderByDeadline()
    }
}