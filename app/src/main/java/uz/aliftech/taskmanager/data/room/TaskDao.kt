package uz.aliftech.taskmanager.data.room

import androidx.room.*
import uz.aliftech.taskmanager.data.model.Task

@Dao
interface TaskDao {
    @Insert
    fun addTask(task: Task)

    @Query("SELECT * FROM tasks ORDER BY deadline ASC")
    fun getAllTasksOrderByDeadline(): List<Task>

    @Query("SELECT * FROM tasks WHERE status = :status ORDER BY deadline ASC")
    fun getTasksByStatusOrderByDeadline(status: Int): List<Task>

    @Update
    fun updateTask(task: Task)

    @Delete
    fun removeTask(task: Task)
}