package uz.aliftech.taskmanager.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tasks")
data class Task(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    var name: String,
    var deadline: Long,
    var status: Int = TODO
) {
    companion object {
        const val TODO = 1
        const val IN_PROCESS = 2
        const val DONE = 3
    }
}
