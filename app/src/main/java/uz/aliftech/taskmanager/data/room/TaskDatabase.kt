package uz.aliftech.taskmanager.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import uz.aliftech.taskmanager.data.model.Task

@Database(entities = [Task::class], version = 1, exportSchema = false)
abstract class TaskDatabase : RoomDatabase() {
    abstract fun dao(): TaskDao
}
