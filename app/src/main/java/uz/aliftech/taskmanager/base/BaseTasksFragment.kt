package uz.aliftech.taskmanager.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import org.koin.android.ext.android.inject
import uz.aliftech.taskmanager.R
import uz.aliftech.taskmanager.databinding.FragmentTasksBinding
import uz.aliftech.taskmanager.ui.all.TaskAdapter

abstract class BaseTasksFragment: Fragment(R.layout.fragment_tasks) {
    lateinit var binding: FragmentTasksBinding
    val adapter: TaskAdapter by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentTasksBinding.bind(view)
        binding.apply {
            taskList.addItemDecoration(MarginItemDecoration(8))
            taskList.adapter = adapter
        }
    }
}