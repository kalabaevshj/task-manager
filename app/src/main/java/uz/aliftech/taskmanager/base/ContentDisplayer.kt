package uz.aliftech.taskmanager.base

interface ContentDisplayer {
    fun display()
}