package uz.aliftech.taskmanager.di

import android.content.Context
import androidx.room.Room
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import uz.aliftech.taskmanager.R
import uz.aliftech.taskmanager.domain.repository.TaskRepositoryImpl
import uz.aliftech.taskmanager.data.room.TaskDatabase
import uz.aliftech.taskmanager.domain.usecase.TaskUseCase
import uz.aliftech.taskmanager.ui.addtask.AddTaskViewModel
import uz.aliftech.taskmanager.ui.all.TaskAdapter
import uz.aliftech.taskmanager.ui.all.AllTasksViewModel
import uz.aliftech.taskmanager.ui.done.TaskDoneViewModel
import uz.aliftech.taskmanager.ui.main.TaskInProcessViewModel

val dataModule = module {
    single {
        androidApplication().applicationContext.getSharedPreferences(
            androidApplication().applicationContext.getString(R.string.package_name),
            Context.MODE_PRIVATE
        )
    }
    single {
        Room.databaseBuilder(
            androidContext(),
            TaskDatabase::class.java,
            androidApplication().applicationContext.getString(R.string.db_name)
        ).build()
    }
    single { get<TaskDatabase>().dao() }
}

val repositoryModule = module {
    single { TaskRepositoryImpl(get()) }
}

val useCaseModule = module {
    single { TaskUseCase(get<TaskRepositoryImpl>()) }
}

val viewModelModule = module {
    viewModel { AddTaskViewModel(get()) }
    viewModel { AllTasksViewModel(get()) }
    viewModel { TaskInProcessViewModel(get()) }
    viewModel { TaskDoneViewModel(get()) }
}

val adapterModule = module {
    single { TaskAdapter() }
}