package uz.aliftech.taskmanager.domain.repository

import uz.aliftech.taskmanager.data.model.Task

interface TaskRepository {
    suspend fun addTask(task: Task)
    suspend fun getAllTasksOrderByDeadline(): List<Task>
    suspend fun getTasksByStatusOrderByDeadline(status: Int): List<Task>
    suspend fun updateTask(task: Task)
    suspend fun removeTask(task: Task)
}