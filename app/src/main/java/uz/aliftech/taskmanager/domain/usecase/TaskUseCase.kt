package uz.aliftech.taskmanager.domain.usecase

import uz.aliftech.taskmanager.data.model.Task
import uz.aliftech.taskmanager.domain.repository.TaskRepository
class TaskUseCase(private val repository: TaskRepository) {
    suspend fun addTask(task: Task) {
        // Business logic will be here if needed
        repository.addTask(task)
    }

    suspend fun getAllTasksOrderByDeadline() = repository.getAllTasksOrderByDeadline()

    suspend fun updateTask(task: Task) = repository.updateTask(task)

    suspend fun getTasksByStatusOrderByDeadline(status: Int) = repository.getTasksByStatusOrderByDeadline(status)

    suspend fun removeTask(task: Task) = repository.removeTask(task)
}