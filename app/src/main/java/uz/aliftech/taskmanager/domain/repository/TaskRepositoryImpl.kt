package uz.aliftech.taskmanager.domain.repository

import uz.aliftech.taskmanager.data.model.Task
import uz.aliftech.taskmanager.data.room.TaskDao

class TaskRepositoryImpl(private val dao: TaskDao) : TaskRepository {
    override suspend fun addTask(task: Task) = dao.addTask(task)

    override suspend fun getAllTasksOrderByDeadline() = dao.getAllTasksOrderByDeadline()

    override suspend fun getTasksByStatusOrderByDeadline(status: Int): List<Task> = dao.getTasksByStatusOrderByDeadline(status)

    override suspend fun updateTask(task: Task) = dao.updateTask(task)

    override suspend fun removeTask(task: Task) = dao.removeTask(task)
}